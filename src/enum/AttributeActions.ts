/**
 * Set actions for entity.
 */
export enum AttributeActions {
    REST = 0,
    CREATE = 1,
    READ = 2,
    UPDATE = 3,
    DELETE = 4,
}