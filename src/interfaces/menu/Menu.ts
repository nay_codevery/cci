import {MenuItem} from "./MenuItem";

export interface MenuInterface<T> {
    // setter + getter
    menu:  {[key: string]: T};
    // setter + getter
    menuItem: MenuItem
}

export interface MenuItemSet<MenuItem> {
    [key: string] : MenuItem;
}

export abstract class Menu implements MenuInterface<MenuItem>{

    private _menu: {[key: string]: MenuItem};

    constructor(mI: MenuItem){
        this._menu = {"main":mI};
    }

    /**
     * Retrieving all menu items
     */
    get menu(): {[key: string]: MenuItem}{
        return this._menu;
    }

    /**
     *Add menu items
     */
    public set menuItem(item: MenuItem) {
        this._menu[item.path] = item;
    }

    public deleteItem(pathName: string) {
        delete  this._menu[pathName];
    }

}
