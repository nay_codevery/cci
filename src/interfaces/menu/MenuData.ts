
export interface MenuData {
    title: string;
    icon: string;
    selected: boolean;
    expanded: boolean;
    order: number;
    hidden: boolean;
}