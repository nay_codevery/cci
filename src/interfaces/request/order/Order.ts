import {AttributeActions} from "../../../enum/AttributeActions";

/**
 * 
 */
export namespace OrderRequest {

    /**
     * Order`s fields. Поля продукта в заказе.
     * OrderId - order`s id.
     * ProductId - product`s id.
     * FinalProviderId - id of user with role provider.
     */
    export interface OrderItemFieldRequestInterface {
        id?: number;
        OrderItemId?: number;
        ProductFieldId: number;
        value: string;
        action: AttributeActions;
    }

    /**
     * Product as order`s item. Продукты в заказе.
     * id - orderItem`s id.
     * OrderId - order`s id.
     * ProductId - product`s id.
     * FinalProviderId - id of user with role provider.
     * orderItemFields[] - array of order`s items.
     */
    export interface OrderItemRequestInterface {
        id?: number;
        OrderId?: number;
        ProductId: number;
        FinalProviderId?: number;
    
        OrderItemFields: Array<OrderRequest.OrderItemFieldRequestInterface>
    }

    /**
     * Attribute of order.
     * id - order`s id.
     * ClientId - id of user (role == Client )
     * ManagerId - id of user (role == manager).
     */
    export interface OrderAttributeRequestInterface {
        id?: number;
        ClientId: number;
        ManagerId?: number;
        status: number;

        orderItems?: Array<OrderRequest.OrderItemRequestInterface>
    }

    /**
     * Описываем заказ.
     * orderAttributes - order`s attributes
     */
    export interface Order {
        orderAttributes: OrderAttributeRequestInterface
    }

}