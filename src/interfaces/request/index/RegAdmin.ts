
export interface RegAdmin {
    login: string;
    mail: string;
    pass: string;
    status?: boolean
}