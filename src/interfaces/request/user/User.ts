
export interface SaveUser {
    login: string;
    mail: string;
    status?: boolean
}

export interface UpdateUser {
    login: string;
    mail: string;
    status?: boolean
}

export interface SoftDeleteUser {
    deleted: boolean
}