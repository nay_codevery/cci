/**
 * If RoleAssignment::RoleId === ""  removes all relationships (roles).
 */
export interface RoleAssignment {
    RoleId?: number | "" ;
}