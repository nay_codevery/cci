
export interface Size {
    id?: number,
    UserToProductId: number,
    tits?: number,
    waistline?: number,
    femur?: number,
    status: number, // 1||0
    options?: string,
    sizeGridId: number,
    sizeStatus: number,
}