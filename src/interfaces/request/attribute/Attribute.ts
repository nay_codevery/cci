
export interface CreateAttribute {
    value?: string;
    UserId: number;
    UserFieldId: number;
}

export interface UpdateAttribute {
    value: string;
}