
export interface ProductAttributes {
    id: number;
    name: string;
    desc?: string;
    sku?: string;
    status?: number;
    price?: number;
    UserId: number;
    ProductTypeId: number;
}