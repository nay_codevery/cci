
/**
 * @field sku - sku у поставщика.
 * @field options - настройки товара, (гриды, ссылки и т.д.)
 */
export interface UserToProductAttr {
    id?: number;
    ProductId: number;
    UserId: number;
    sku?: string,
    options?: string,  // JSON
}
