/**
 * action - имя действие (получаем по GET:/permission/data)
 * resource - имя ресурса (получаем по GET:/permission/data)
 * attr_set - имя атрибутов (получаем по GET:/permission/data)
 */
export interface Permission {
    id?: number,
    RoleId:number,
    resource: string,
    action: string,
    attr_set: string,
    options?: string,
    description?: string,
}