
export interface CreateRole {
    name: string
    visible?: boolean;
}

export interface UpdateRole {
    id: number;
    name?: string
    visible?: boolean;
}