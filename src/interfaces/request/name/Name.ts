
export interface Name {
    id?: number,
    name: string,
    is_ban: boolean,
    gender: boolean
}

export interface EditName extends Name {
    id: number,
}

export interface CortegeNameKey {
    ProductTypeId: string;
    MaterialId: string;
}

export interface ProductName extends CortegeNameKey {
    id?: number;
    ProductId?: number;
    NameId: number;
    NameString: string;
}

export interface EditProductName extends ProductName {
    id: number;
}
