
export interface Price {
    id?: number,
    UserToProductId: number,
    CurrencyId: number,
    Purchase: number,
    rrc: number,
    csp: number,
    Fixable: boolean,
    OldPrice: number,
    ExtraCharge: number,
    ExtraChargeSum: number,   
    Retail: number,
    FixedPrice: number,
    FixedExtraCharge: number,
}