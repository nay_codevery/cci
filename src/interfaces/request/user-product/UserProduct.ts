/**
 * interface for answer object
 * userProductRelation - this property conteins <EditUserProductRelation | CreateUserProductRelation>
 */
export interface UserProductRelationRequest {
    userProductRelation:  EditUserProductRelation | CreateUserProductRelation;
}


/**
 * id - id of relation.
 */
export interface EditUserProductRelation {
    
    id: number;
    UserId: number;
    ProductId: number;
}

/**
 * UserId - 
 * ProductId -
 */
export interface CreateUserProductRelation {
    UserId: number;
    ProductId: number;
}