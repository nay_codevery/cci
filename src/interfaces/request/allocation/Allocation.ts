/**
 * @field option string - не испоьзуется, на момент создания добавлялось как проЗапас.
 * @field deleted boolean - по умолчанию false, true для мягкого удаления
 */
export interface AllocationTargetField {
    id?: number;
    name: string;
    option?: string;
    deleted?: boolean;
}