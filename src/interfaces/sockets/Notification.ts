
/**
 * id - id of relation.
 */
export interface ReadNotification {
    UserId: number;
}

/**
 * is - read
 * type - 'newNotice' type of serverSocketEvent   server.socket.emmit('newNotice', Notification)
 */
export interface UpdateNotification {
    id: number;
    UserId: number;
    isRead: boolean;
    type: string;
    data: string;
}