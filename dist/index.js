"use strict";
/* ------------------------------------ ENUMS --------------------------------- */
Object.defineProperty(exports, "__esModule", { value: true });
var AttributeActions_1 = require("./enum/AttributeActions");
exports.AttributeActions = AttributeActions_1.AttributeActions;
var OrderStatuses = require("./interfaces/request/order/OrderStatus");
exports.OrderStatuses = OrderStatuses;
var OrderContractorStatus = require("./interfaces/request/chat/OrderContractorStatus");
exports.OrderContractorStatus = OrderContractorStatus;
var OrderContractor = require("./interfaces/request/chat/OrderContractor");
exports.OrderContractor = OrderContractor;
var TalkMessages = require("./interfaces/request/chat/OrderContractor");
exports.TalkMessages = TalkMessages;
/** ------------------------------------ MENU ---------------------------------- */
/*
import {MenuInterface, Menu} from './interfaces/menu/Menu';
import {MenuItem} from './interfaces/menu/MenuItem';
import {MenuData} from './interfaces/menu/MenuData';

export type MenuStatic = [ MenuInterface<MenuItem>, Menu, MenuItem, MenuData ];*/
/** ------------------------- user-product-relation ---------------------------- */
/*
import {UserProductRelationRequest, CreateUserProductRelation, EditUserProductRelation} from "./interfaces/request/user-product/UserProduct";
export {UserProductRelationRequest, CreateUserProductRelation, EditUserProductRelation};*/
/** -----------------########-------------------- SOCKETS ------------------#######--------------------- */
/* ------------------------------------------ Notification -------------------------------------------- */
var NotificationNS = require("./interfaces/sockets/Notification");
exports.NotificationNS = NotificationNS;
/* ------------------------------------------ Notification -------------------------------------------- */
var MessegeRequest = require("./interfaces/sockets/Message");
exports.MessegeRequest = MessegeRequest;
var SizeStatuses = /** @class */ (function () {
    function SizeStatuses() {
        this.statuses = [];
        this.statuses.push({ id: 1, name: "НЕТ В НАЛИЧИИ", colorCode: "#BDBDBD" });
        this.statuses.push({ id: 3, name: "НЕ СУЩЕСТВУЕТ", colorCode: "#FF0000" });
        this.statuses.push({ id: 2, name: "ЕСТЬ В НАЛИЧИИ", colorCode: "#00FF00" });
        this.statuses.push({ id: 4, name: "стоит у поставщика галка Архивный", colorCode: "#6C6C6C" });
        this.statuses.push({ id: 5, name: "статус только для ШР", colorCode: "#009933" });
        this.statuses.push({ id: 6, name: "нет в наличии сейчас", colorCode: "#FF9933" });
    }
    SizeStatuses.prototype.getStatuses = function () {
        return this.statuses;
    };
    return SizeStatuses;
}());
exports.SizeStatuses = SizeStatuses;
