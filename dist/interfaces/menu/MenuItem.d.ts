import { MenuData } from "./MenuData";
export interface MenuItem {
    path: string;
    data: MenuData;
    children: Array<MenuItem>;
}
