"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Menu = /** @class */ (function () {
    function Menu(mI) {
        this._menu = { "main": mI };
    }
    Object.defineProperty(Menu.prototype, "menu", {
        /**
         * Retrieving all menu items
         */
        get: function () {
            return this._menu;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Menu.prototype, "menuItem", {
        /**
         *Add menu items
         */
        set: function (item) {
            this._menu[item.path] = item;
        },
        enumerable: true,
        configurable: true
    });
    Menu.prototype.deleteItem = function (pathName) {
        delete this._menu[pathName];
    };
    return Menu;
}());
exports.Menu = Menu;
