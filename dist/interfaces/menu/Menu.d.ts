import { MenuItem } from "./MenuItem";
export interface MenuInterface<T> {
    menu: {
        [key: string]: T;
    };
    menuItem: MenuItem;
}
export interface MenuItemSet<MenuItem> {
    [key: string]: MenuItem;
}
export declare abstract class Menu implements MenuInterface<MenuItem> {
    private _menu;
    constructor(mI: MenuItem);
    /**
     * Retrieving all menu items
     */
    readonly menu: {
        [key: string]: MenuItem;
    };
    /**
     *Add menu items
     */
    menuItem: MenuItem;
    deleteItem(pathName: string): void;
}
