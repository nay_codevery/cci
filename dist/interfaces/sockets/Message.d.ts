/**
 * id - id of relation.
 */
export interface ReadMessage {
    id?: number;
    OrderContractorId?: number;
    UserId?: number;
    UserIdFrom?: number;
    is_read?: boolean;
}
/**
 * is - read
 * type - 'newNotice' type of serverSocketEvent   server.socket.emmit('newNotice', Notification)
 */
export interface UpdateMessage {
    id: number;
    is_read?: boolean;
    message?: string;
}
export interface DeleteMessage {
    id: number;
}
export interface CreateMessage {
    OrderContractorId?: number;
    UserId?: number;
    UserIdFrom?: number;
    message?: string;
}
