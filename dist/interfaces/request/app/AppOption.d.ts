export interface AppOption {
    id?: number;
    name?: string;
    options?: string;
}
