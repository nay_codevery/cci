/**
 * interface for answer object
 * chatStatus - this property conteins <ReadOrderStatus | EditOrderStatus | CreateOrderStatus>
 */
export interface OrderStatusRequest<T> {
    orderStatus: ReadOrderStatus | EditOrderStatus | CreateOrderStatus;
}
/**
 * name - order`s status name.
 */
export interface OrderStatus {
    name: string;
}
/**
 * id - id of order`s status.
 */
export interface ReadOrderStatus extends OrderStatus {
    id: number;
}
/**
 * id - id of order`s status.
 */
export interface EditOrderStatus extends OrderStatus {
    id: number;
}
/**
 *
 */
export interface CreateOrderStatus extends OrderStatus {
}
