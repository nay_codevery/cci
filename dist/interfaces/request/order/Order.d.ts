import { AttributeActions } from "../../../enum/AttributeActions";
/**
 *
 */
export declare namespace OrderRequest {
    /**
     * Order`s fields. Поля продукта в заказе.
     * OrderId - order`s id.
     * ProductId - product`s id.
     * FinalProviderId - id of user with role provider.
     */
    interface OrderItemFieldRequestInterface {
        id?: number;
        OrderItemId?: number;
        ProductFieldId: number;
        value: string;
        action: AttributeActions;
    }
    /**
     * Product as order`s item. Продукты в заказе.
     * id - orderItem`s id.
     * OrderId - order`s id.
     * ProductId - product`s id.
     * FinalProviderId - id of user with role provider.
     * orderItemFields[] - array of order`s items.
     */
    interface OrderItemRequestInterface {
        id?: number;
        OrderId?: number;
        ProductId: number;
        FinalProviderId?: number;
        OrderItemFields: Array<OrderRequest.OrderItemFieldRequestInterface>;
    }
    /**
     * Attribute of order.
     * id - order`s id.
     * ClientId - id of user (role == Client )
     * ManagerId - id of user (role == manager).
     */
    interface OrderAttributeRequestInterface {
        id?: number;
        ClientId: number;
        ManagerId?: number;
        status: number;
        orderItems?: Array<OrderRequest.OrderItemRequestInterface>;
    }
    /**
     * Описываем заказ.
     * orderAttributes - order`s attributes
     */
    interface Order {
        orderAttributes: OrderAttributeRequestInterface;
    }
}
