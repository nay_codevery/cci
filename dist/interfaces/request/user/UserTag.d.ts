export interface CreateUserTagAttributes {
    name: string;
    UserId: number;
}
export interface UpdateUserTagAttributes {
    name: string;
    value?: string;
}
