/**
 * If RoleAssignment::RoleId === ""  removes all relationships (roles).
 */
export interface TagRelations {
    UserTagId?: number;
}
