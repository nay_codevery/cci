export interface UserFieldData {
    id?: number;
    UserFieldId: number;
    UserId: number;
    value?: String;
}
