/**
 * При создаении поля, назначаем ему отношение к роли через массив roles [ 1, 2 .. :roleId ]
 */
export interface CreateUserField {
    name: string;
    type: string;
    values?: string;
    roles?: number[];
}
export interface UpdateUserField {
    id: number;
    name: string;
    type: string;
    values?: string;
}
