export interface CategoryAttributes {
    id?: number;
    name: string;
    desc?: string;
    ParentId?: string;
}
