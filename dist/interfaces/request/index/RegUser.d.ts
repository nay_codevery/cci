export interface RegUser {
    login: string;
    mail: string;
    pass: string;
    status?: boolean;
}
