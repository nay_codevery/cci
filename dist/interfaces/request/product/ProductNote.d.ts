export interface ProductNote {
    id?: number;
    ProductId: number;
    UserId: number;
    text?: string;
    status?: boolean;
}
