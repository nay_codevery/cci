export interface ProductField {
    id: number;
    name: string;
    type?: string;
    as_variation?: boolean;
    applicable_to_all: number;
    values?: string;
    order_item?: number;
    allocation?: number;
}
