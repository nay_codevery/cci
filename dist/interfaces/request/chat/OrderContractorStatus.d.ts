/**
 * interface for answer object
 * chatStatus - this property conteins <ReadOrderStatus | EditOrderStatus | CreateOrderStatus>
 */
export interface ChatStatusRequest<T> {
    chatStatus: ReadOrderContractorStatus | EditOrderContractorStatus | CreateOrderContractorStatus;
}
/**
 * name - name of orders.
 */
export interface OrderContractorStatus {
    name: string;
}
/**
 * id - id of status.
 */
export interface ReadOrderContractorStatus extends OrderContractorStatus {
    id: number;
}
/**
 * id - id of status.
 */
export interface EditOrderContractorStatus extends OrderContractorStatus {
    id: number;
}
/**
 *
 */
export interface CreateOrderContractorStatus extends OrderContractorStatus {
}
