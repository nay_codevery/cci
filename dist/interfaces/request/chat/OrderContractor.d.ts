/**
 * Request Chat Body.
 */
export interface ChatRequest<T> {
    chat: ReadOrderContractor | EditOrderContractor | CreateOrderContractor;
}
/**
 * price - product`s price.
 * talk_status - chats
 */
export interface OrderContractor {
    price: number;
    talkStatus: number;
}
/**
 * id - chat`s id.
 * OrderItemId - id of product`s item.
 * UserId - User`s id.
 */
export interface ReadOrderContractor extends OrderContractor {
    id: number;
    OrderItemId: number;
    UserId: number;
}
/**
 * id - chat`s id.
 */
export interface EditOrderContractor extends OrderContractor {
    id: number;
}
/**
 * OrderItemId - id of product`s item.
 * UserId -User`s id.
 */
export interface CreateOrderContractor extends OrderContractor {
    OrderItemId: number;
    UserId: number;
}
