/**
 * message - content of message.
 */
export interface TalkMessages {
    message?: string;
    is_read: boolean;
}
/**
 * id - message`s id.
 * UserIdFrom - from message.
 * UserId - addressee.
 * is_read - status, read or don`t read message.
 */
export interface ReadTalkMessages extends TalkMessages {
    id: number;
    UserIdFrom: number;
    UserId: number;
}
/**
 * id - message`s id.
 * message - content of message.
 */
export interface EditTalkMessages extends TalkMessages {
    id: number;
    is_read: boolean;
}
/**
 * UserId - addressee.
 * OrderContractorId - id of order chat.
 */
export interface CreateTalkMessages extends TalkMessages {
    UserId: number;
    OrderContractorId: number;
}
