/**
 *
 * ChildId - читать как parentId.
 */
export interface CreateRoleChild {
    RoleId?: number;
    ChildId: number;
}
/**
 *
 * ChildId - читать как parentId.
 */
export interface UpdateRoleChild {
    RoleId?: number;
    ChildId: number;
}
