/**
 * Базoвая наценка продукта (конкретного в отношении).
 */
export interface BasicProductMargin {
    value: number;
}
