"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
* Типы полей для пользователя.
*/
var UserFieldTypes;
(function (UserFieldTypes) {
    UserFieldTypes["TEXT"] = "text";
    UserFieldTypes["CHECKBOX"] = "checkbox";
    UserFieldTypes["RADIO"] = "radio";
    UserFieldTypes["TEXTAREA"] = "textarea";
    UserFieldTypes["EMAIL"] = "email";
    UserFieldTypes["SELECT"] = "select";
    UserFieldTypes["IMAGE_UPLOAD"] = "imageUpload";
    UserFieldTypes["TEL"] = "tel";
    UserFieldTypes["NUMBER"] = "number";
    UserFieldTypes["SWITCH_1"] = "switch_1";
    UserFieldTypes["VIDEO"] = "video";
})(UserFieldTypes = exports.UserFieldTypes || (exports.UserFieldTypes = {}));
/**
 * Типы полей для продукта
 */
var ProductFieldTypes;
(function (ProductFieldTypes) {
    ProductFieldTypes["TEXT"] = "text";
    ProductFieldTypes["NUMBER"] = "number";
    ProductFieldTypes["IMAGE"] = "image";
    ProductFieldTypes["FILE"] = "file";
    ProductFieldTypes["COLOR"] = "color";
    ProductFieldTypes["IMAGE_UPLOAD"] = "imageUpload";
    ProductFieldTypes["SWITCH_1"] = "switch_1";
    ProductFieldTypes["VIDEO"] = "video";
})(ProductFieldTypes = exports.ProductFieldTypes || (exports.ProductFieldTypes = {}));
