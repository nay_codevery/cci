/**
* Типы полей для пользователя.
*/
export declare enum UserFieldTypes {
    TEXT = "text",
    CHECKBOX = "checkbox",
    RADIO = "radio",
    TEXTAREA = "textarea",
    EMAIL = "email",
    SELECT = "select",
    IMAGE_UPLOAD = "imageUpload",
    TEL = "tel",
    NUMBER = "number",
    SWITCH_1 = "switch_1",
    VIDEO = "video",
}
/**
 * Типы полей для продукта
 */
export declare enum ProductFieldTypes {
    TEXT = "text",
    NUMBER = "number",
    IMAGE = "image",
    FILE = "file",
    COLOR = "color",
    IMAGE_UPLOAD = "imageUpload",
    SWITCH_1 = "switch_1",
    VIDEO = "video",
}
