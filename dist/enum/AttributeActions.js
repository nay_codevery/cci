"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Set actions for entity.
 */
var AttributeActions;
(function (AttributeActions) {
    AttributeActions[AttributeActions["REST"] = 0] = "REST";
    AttributeActions[AttributeActions["CREATE"] = 1] = "CREATE";
    AttributeActions[AttributeActions["READ"] = 2] = "READ";
    AttributeActions[AttributeActions["UPDATE"] = 3] = "UPDATE";
    AttributeActions[AttributeActions["DELETE"] = 4] = "DELETE";
})(AttributeActions = exports.AttributeActions || (exports.AttributeActions = {}));
