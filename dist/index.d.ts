export { AttributeActions } from './enum/AttributeActions';
/** orders interfaces */
export { OrderRequest } from './interfaces/request/order/Order';
import * as OrderStatuses from './interfaces/request/order/OrderStatus';
export { OrderStatuses };
import * as OrderContractorStatus from './interfaces/request/chat/OrderContractorStatus';
export { OrderContractorStatus };
import * as OrderContractor from './interfaces/request/chat/OrderContractor';
export { OrderContractor };
import * as TalkMessages from './interfaces/request/chat/OrderContractor';
export { TalkMessages };
/** ------------------------------------ MENU ---------------------------------- */
/** ------------------------- user-product-relation ---------------------------- */
/** -----------------########-------------------- SOCKETS ------------------#######--------------------- */
import * as NotificationNS from './interfaces/sockets/Notification';
export { NotificationNS };
import * as MessegeRequest from './interfaces/sockets/Message';
export { MessegeRequest };
import { RegAdmin as regAdmin } from "./interfaces/request/index/RegAdmin";
import { RegUser as regUser } from "./interfaces/request/index/RegUser";
import { SaveUser as createUser, UpdateUser as updateUser } from "./interfaces/request/user/User";
import { UserFieldData } from "./interfaces/request/user/UserFieldData";
import { RoleAssignment } from "./interfaces/request/user/RoleAssignment";
import { TagRelations } from "./interfaces/request/user/TagRelations";
import { CreateAttribute, UpdateAttribute } from "./interfaces/request/attribute/Attribute";
import { CreateUserTagAttributes, UpdateUserTagAttributes } from "./interfaces/request/user/UserTag";
import { CreateUserField, UpdateUserField } from "./interfaces/request/user/UserField";
import { CreateRoleChild } from "./interfaces/request/access/roleChild/RoleChild";
import { CreateRole, UpdateRole } from "./interfaces/request/access/role/Role";
import { AllocationTargetField } from "./interfaces/request/allocation/Allocation";
import { CategoryAttributes } from "./interfaces/request/category/Category";
import { ChatRequest, CreateOrderContractor, EditOrderContractor } from './interfaces/request/chat/OrderContractor';
import { ChatStatusRequest, CreateOrderContractorStatus, EditOrderContractorStatus } from './interfaces/request/chat/OrderContractorStatus';
import { Price } from "./interfaces/request/price/Price";
import { Currency } from "./interfaces/request/currency/Currency";
import { OrderRequest } from "./interfaces/request/order/Order";
import { OrderStatusRequest, CreateOrderStatus, EditOrderStatus } from "./interfaces/request/order/OrderStatus";
import { Permission } from "./interfaces/request/access/permission/Permission";
import { ProductFieldData } from "./interfaces/request/product/ProductFieldData";
import { ProductField } from "./interfaces/request/product/ProductField";
import { ProductNote } from "./interfaces/request/product/ProductNote";
import { ProductAttributes } from "./interfaces/request/product/Product";
import { ProductType } from "./interfaces/request/product/ProductType";
import { UserToProductAttr } from "./interfaces/request/product/UserToProduct";
import { AppOption } from "./interfaces/request/app/AppOption";
import { Size } from "./interfaces/request/size/SizeOption";
import { Name, CortegeNameKey, EditProductName, ProductName } from "./interfaces/request/name/Name";
export declare namespace Request {
    namespace Index {
        interface RegAdmin extends regAdmin {
        }
        interface RegUser extends regUser {
        }
    }
    /**
     * Действия над содержимим пользовательских филдов(полей), не путать с атрибутами пользователя.
     * Attribute читать как => UserFieldData.
     *
     */
    namespace Attribute {
        /**
         * Чтение содержимого филда, GET:/attribute/:id  или всех филдов GET:/attribute/list
         */
        interface Read extends Create {
        }
        /**
         * Создание данных для определенного поля(филда)
         */
        interface Create {
            userAttribute: CreateAttribute;
        }
        /**
         * Обновление данных пользовательского поля.
         */
        interface Update {
            userAttribute: UpdateAttribute;
        }
        /**
         * Удаление данных пользовательского поля.
         */
        interface Delete {
        }
    }
    /**
     * Действие нд расположения филда(поля,атрибута) продукта.
     * "Тип" полей(продукта) по назаначению  (сео, гамнео и т.д.)
     *
     */
    namespace AllocationFieldTarget {
        /**
         * POST:/alloc/
         */
        interface Create {
            AllocationTargetFieldAttributes: AllocationTargetField;
        }
        /**
         * GET:/alloc/:id OR /alloc/list
         */
        interface Read {
        }
        /**
         * PUT:/alloc/:id
         */
        interface Update extends Create {
        }
        /**
         * DELETE:/alloc/:id
         * DELETE:/alloc-hard/:id
         */
        interface Delete {
        }
    }
    /**
     * Действие над категориями
     * GET:/category/list
     * GET:/category/:id
     * POST:/category
     * PUT:/category/:id
     * DELETE:/category/:id
     */
    namespace Category {
        interface Create {
            categoryAttributes: CategoryAttributes;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над переговорами...
     * GET:/chat/list
     * GET:/chat-users/:id - получить собиседников по чату , возвратит {contractorUserId:number, ManagerId:number}
     * GET:/chat-users/:id - id of chat
     * GET:/chat-by-manager-id/:id - чаты по ид менеджера.
     * POST:/chat
     * PUT:/chat/:id
     * DELETE:/chat/:id
     */
    namespace Chat {
        interface Create extends ChatRequest<CreateOrderContractor> {
        }
        interface Read {
        }
        interface Update extends ChatRequest<EditOrderContractor> {
        }
        interface Delete {
        }
    }
    /**
     * Действие над статусами переговоров.
     * GET:/chat-status/list
     * GET:/chat-status/:id
     * POST:/chat-status
     * PUT:/chat-status/:id
     * DELETE:/chat-status/:id
     */
    namespace ChatStatuses {
        interface Create extends ChatStatusRequest<CreateOrderContractorStatus> {
        }
        interface Read {
        }
        interface Update extends ChatStatusRequest<EditOrderContractorStatus> {
        }
        interface Delete {
        }
    }
    /**
     * Действие над курсами валют...
     * GET:/currency/list
     * GET:/currency/:id
     * POST:/currency
     * PUT:/currency/:id
     * DELETE:/currency/:id
     */
    namespace Currency {
        interface Create {
            currency: Currency;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над заказом...
     * GET:/order/list
     * GET:/order/:id
     * POST:/order
     * PUT:/order/:id
     * DELETE:/order/:id
     */
    namespace Order {
        interface Create extends OrderRequest.Order {
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над статусом заказа...
     * GET:/order-status/list
     * GET:/order-status/:id
     * POST:/order-status
     * PUT:/order-status/:id
     * DELETE:/order-status/:id
     */
    namespace OrderStatus {
        interface Create extends OrderStatusRequest<CreateOrderStatus> {
        }
        interface Read {
        }
        interface Update extends OrderStatusRequest<EditOrderStatus> {
        }
        interface Delete {
        }
    }
    /**
     * Действие над разрешениями для полей.
     */
    namespace Permission {
        interface Create {
            permission: Permission;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над ценой...
     * GET:/price/list
     * GET:/price/:id
     * GET:/price/:UserToProductId/list  - получить цены определенного отношения поставщика к продукту.
     * POST:/price
     * PUT:/price/:id
     * DELETE:/price/:id
     */
    namespace Price {
        interface Create {
            price: Price;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над данними полей продукта...
     * GET:/p_field_data/list
     * GET:/p_field_data/:id
     * GET:/p_field_data/listProductId?filter[KKKKK]=VVVVV&readAll=1
     *      readAll = 1|0  - (все или первая запись),
     *      KKKKK  - имя филда по которому ищем,
     *      VVVVV  - значение филда,
     *      filter[KKKKK_1]=VVVVV_1&filter[KKKKK_2]=VVVVV_2&filter[KKKKK_3]=VVVVV_3  - так тоже можно.
     * POST:/p_field_data
     * PUT:/p_field_data/:id
     * DELETE:/p_field_data/:id
     */
    namespace ProductFieldData {
        interface Create {
            pFieldData: ProductFieldData;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над полями продукта...
     * GET:/product_field/list
     * GET:/product_field/:id
     * POST:/product_field
     * POST:/products-by-array - в теле запроса id нужных товаров, запрос возвращает полные товары
     * PUT:/product_field/:id
     * PUT:/product_field_set_order/:id - его порядок в списке { order: number }
     * PUT:/product_field_set_alloc/:id - его расположение по ГАМНЕО { pFieldAttributes: {allocation: ид группы} }
     * DELETE:/product_field/:id
     */
    namespace ProductField {
        interface Create {
            pFieldAttributes: ProductField;
            productFieldProductType: Array<number>;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface setOrder {
            order: number;
        }
        interface updateAllocation {
            pFieldAttributes: {
                allocation: number;
            };
        }
        interface Delete {
        }
    }
    /**
     * !!!!!!!!!! не реализован на фронте , заказчик отказался, на беке есть.
     * Действие над заметками продукта
     * GET:/product-note/list
     * GET:/product-note/:productId/list - заметки конкретного продукта.
     * GET:/product-note/:id
     * POST:/product-note
     * PUT:/product-note/:id
     * DELETE:/product-note/:id
     */
    namespace ProductNote {
        interface Create {
            note: ProductNote;
        }
        interface Read extends Create {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     *
     * Действие над продуктом
     * GET:/product/list
     * GET:/product/by-user/:id - список продуктов по пользователю.
     * GET:/product-note/:id
     * POST:/product-note
     * POST:/products-by-array      { ids:Array<number> } - набор идентификаторов для поиска.
     * PUT:/product-note/:id
     * DELETE:/product-note/:id
     */
    namespace Product {
        /**
         * @field productAttributes - аттрибуты продукта.
         * @field catsId - массив с идентификаторами категорий, которые нужно прикрепить, если массив пуст то товар будет откреплен от категорий которых нет.
         * @field tagsId - массив с идентификаторами тегов, которые нужно прикрепить, если массив пуст то товар будет откреплен от тегов которых нет.
         * @field UserToProdRelationsID - массив с идентификаторами пользователей,
         *  которых нужно прикрепить, если массив пуст то товар будет откреплен от пользователей которых нет (не факт!!!).
         * @field productFieldData - массив значений полей продукта, если пуст то удаляет данные полей продукта.
         */
        interface Create {
            productAttributes: ProductAttributes;
            catsId: number[];
            tagsId: number[];
            UserToProdRelationsID?: number[];
            productFieldData?: Array<ProductFieldData>;
        }
        interface Read {
        }
        interface readByArray {
            ids: number[];
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действие над типами продукта
     * GET:/product_type/list
     * GET:/product_type/:id
     * POST:/product_type
     * PUT:/product_type/:id
     * DELETE:/product_type/:id
     */
    namespace ProductType {
        interface Create {
            pTypeAttributes: ProductType;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действия над ролями.
     */
    namespace Role {
        /**
         * Чтение содержимого роли, GET:/role/:id  или всех ролей GET:/role/list
         */
        interface Read {
        }
        /**
         * Создание роли. POST:/role
         */
        interface Create {
            roleAttributes: CreateRole;
            roleChildAttributes: Array<CreateRoleChild>;
        }
        /**
         * Обновление данных роли. PUT:/role/:id
         */
        interface Update {
            roleAttributes: UpdateRole;
            roleChildAttributes: Array<CreateRoleChild>;
        }
        /**
         * Удаление роли. DELETE:/role/:id
         */
        interface Delete {
        }
    }
    /** НЕ ОПИСАН */
    namespace Upload {
        interface Create {
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
     * Действия над содержимим пользовательских полей/филдов.
     */
    namespace UsereField {
        /**
         * Чтение содержимого поля, GET:/ufield/:id  или всех филдов GET:/utag/list
         */
        interface Read {
        }
        /**
         * Создание тега. POST:/ufield
         */
        interface Create {
            uFields: CreateUserField;
        }
        /**
         * Обновление данных тега. PUT:/ufield/:id
         */
        interface Update {
            uFields: UpdateUserField;
        }
        /**
         * Удаление данных пользовательского тега. DELETE:/ufield/:id
         */
        interface Delete {
        }
    }
    /**
     * Действие над пользователем по роуту /user
     */
    namespace User {
        /**
         * Получить пользователя, GET:/user/:id, GET:/user/list
         */
        interface Read extends Create {
        }
        /**
         * Создайте пользователя. POST:/user
         * if UserFieldData === "" - removes all userFieldData, if UserFieldData === undefined (если не отправлены) - пропускаем...
         * if UserTagRelations === "" - removes all userFieldData, if UserTagRelations === undefined (если не отправлены) - пропускаем...
         * if AssigmentRoleAttributes === "" || if langth = 0 - removes all userFieldData, if AssigmentRoleAttributes === undefined (если не отправлены) - пропускаем...
         */
        interface Create {
            userAttributes: createUser;
            UserFieldData?: Array<UserFieldData> | "";
            AssigmentRoleAttributes?: Array<RoleAssignment> | "";
            UserTagRelations?: Array<TagRelations> | "";
        }
        /**
         * Редактирование пользователя. PUT:/user/:id
         * if UserFieldData === "" - removes all userFieldData, if UserFieldData === undefined (если не отправлены) - пропускаем...
         * if UserTagRelations === "" - removes all userFieldData, if UserTagRelations === undefined (если не отправлены) - пропускаем...
         * if AssigmentRoleAttributes === "" - removes all userFieldData, if AssigmentRoleAttributes === undefined (если не отправлены) - пропускаем...
         */
        interface Update extends updateUser {
            userAttributes: updateUser;
            UserFieldData?: Array<UserFieldData> | "";
            AssigmentRoleAttributes?: Array<RoleAssignment> | "";
            UserTagRelations?: Array<TagRelations> | "";
        }
        /**
         * "soft" удаление пользователя, DELETE:/user/:id
         */
        interface Delete {
        }
    }
    /**
     * Действия над содержимим пользовательских тегов.
     */
    namespace UserTag {
        /**
         * Чтение содержимого тега, GET:/utag/:id  или всех филдов GET:/utag/list
         */
        interface Read {
        }
        /**
         * Создание тега. POST:/utag
         */
        interface Create {
            tagAttributes: CreateUserTagAttributes;
        }
        /**
         * Обновление данных тега. PUT:/utag/:id
         */
        interface Update {
            tagAttributes: UpdateUserTagAttributes;
        }
        /**
         * Удаление данных пользовательского тега. DELETE:/utag/:id
         */
        interface Delete {
        }
    }
    /**
     * Действие над отношением пользователя к продукту
     * в запросе ниже, XXXX = UserId || ProductId для выборки по пользователю или продукту
     * GET:/product_type/list  ||   /product_type/list?filter[columnName]=XXXX&filter[value]=YYYY&filter[all]=all
     * GET:/product_type/:id
     * POST:/product_type
     * PUT:/product_type/:id
     * DELETE:/product_type/:id
     */
    namespace UserToProduct {
        interface Create {
            userProductRelation: UserToProductAttr;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
    * Действие над настройками приложения , таблица application_options
        GET	/app-option/list/	список опций
        GET	/app-option/list/:name	список опций по имени
        GET	/app-option/:id	опция по ид
        GET	/app-option/:id/:name	опция по ид и имени
        GET	/app-option-last/:name	последняя по имени
        POST /app-option
        PUT	/app-option-by-id/:id
        PUT	/app-option-by-name/:name
        DELETE	/app-option/:id
    */
    namespace AppOption {
        interface Create {
            option: AppOption;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface Delete {
        }
    }
    /**
    * Действие над размерами
        GET	/size/list	список размеров.
        GET	/size/:id	размер по ид.
        POST /size
        PUT	/size/:id
        PUT /size-edit изменяем размеры партией.
        DELETE /size/:id
    */
    namespace Size {
        interface Create {
            size: Size;
        }
        interface Read {
        }
        interface Update extends Create {
        }
        interface UpdateBulk {
            sizeKit: Size[];
        }
        interface Delete {
        }
    }
    /**
    * Действие над именами
        GET	/name/list	список имен.
        GET	/name/:id	имя по ид.
        POST /name
        PUT	/name/:id
        PUT	/name-to-ban/:id поместить имя в бан
        PUT	/name-from-ban/:id вынять с бана
        DELETE /name/:id
    */
    namespace Name {
        interface Create {
            nameArray: Name[];
        }
        interface Read {
        }
        interface Update {
            name: Name;
        }
        interface Delete {
        }
    }
    /**
    * Действие над именами прикрепленными к продукту
        GET	    /product-name/list	список имен для продуков.
        GET	    /product-name/:id	имя по ид.
        POST    /product-name-generate	отправляем кортеж на генерацию имен, получаем кортеж имен
        POST    /product-name-save	добавляем продукт ид к кортежу и сохраняем имя
        PUT	    /product-name-save/:id изменяем имя по идентификатору
        DELETE  /product-name-save/:id
    */
    namespace ProductName {
        interface Generate {
            nameKeyArray: CortegeNameKey[];
        }
        interface Create {
            productNames: ProductName[];
        }
        interface Read {
        }
        interface Update {
            productName: EditProductName;
        }
        interface Delete {
        }
    }
}
/**
 * Все опции в приложении..
 */
export declare namespace Options {
    /**
     * Опции уровня приложения. Пишутся в psql application_options колонка options как JSON строка.
     */
    interface ApplicationOptions {
        [key: string]: any[];
    }
    /**
     * Опции которые находятся в колонке options в отношении пользователя(постовщика) к продукту.
     */
    interface UserToProductOptions {
        link?: string;
        grids?: any;
    }
}
/**
 * Интерфейс размерных сеток
 */
export declare namespace GridSize {
    /**
     * Значение сетки
     * {name:'38',tits:90, waistline:60, femur:60}
     */
    interface Size {
        name: string;
        tits: number;
        waistline: number;
        femur: number;
    }
    /**
     * Сама сетка
     * { "гост": [{name:'38',tits:90, waistline:60, femur:60},{name:'38',tits:90, waistline:60, femur:60},{name:'38',tits:90, waistline:60, femur:60} ]}
     */
    interface Grid {
        value: Size[];
        name: string;
    }
    interface SizeStatus {
        id: number;
        name: string;
        colorCode: string;
    }
}
export declare class SizeStatuses {
    private statuses;
    constructor();
    getStatuses(): GridSize.SizeStatus[];
}
